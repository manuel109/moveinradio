# Movinradio Backend

## Description

This small backend provides the ability to manage playlists and tracks with minimal features :
- User
  - register with a username and password
  - login with a username and password
- Playlists (only for self (playlist owner = logged-in user))
  - create a playlist with a title
  - edit a playlist (modify title, update tracks list)
  - delete a playlist
  - get playlists
- Tracks
  - create/update/delete tracks (author, album, title)
  - list tracks with pagination

## Technical details

### Database

The POSTGRESQL database is initialized using Liquibase to create tables and populate tracks list. To avoid sequence issues, initially created tracks use an id starting with 10000 (100001 to 1000040). In a real project this would have to be handled in a better way.

### Security

Everything concerning security is in package `security` and is created using spring-security implementations. The password is stored encrypted in the database using BCrypt algorithm. Two filters are applied :

- a `JwtAuthTokenFilter` implemented in the project
- a spring-based `UsernamePasswordAuthenticationFilter`

### General architecture

- all REST endpoints and there associated requests/responses are in package `controllers`
  - `AuthController` contains `signup` and `signin` endpoints
  - `UserController` contains create/get playlist endpoints
  - `PlaylistController` contains update/delete playlist endpoints
  - `TrackController` contains CRUD tracks endpoints
- all orm related objects (entities, repositories) are in package `orm`
- all services in the `services` package

### Unit testing

- only tracks endpoints are tested through unit testing using `RestAssured` framework, others were tested using a Postman collection (see [Postman collection](#postman-collection))


## Prerequisites

Have [Docker](https://www.docker.com), [Maven 3.8+](https://maven.apache.org) and [Java 17+](https://adoptium.net) installed

Copy `./.env.dev` to `./.env`

## Development

Start development infrastructure (database) with:

```bash
docker compose up -d
```
or (depending on your version)

```bash
docker-compose up -d
```

Run the project with:

```bash
mvn clean spring-boot:run
```

Stop development infrastructure (database) with:

```bash
docker compose down
```
or (depending on your version)

```bash
docker-compose down
```

Run tests with:

```bash
mvn clean verify -U
```


## Postman collection

To have the requests working first use `Register user` then `User sign-in` and all necessary data will be stored in the environment.
