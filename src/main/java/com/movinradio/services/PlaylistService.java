package com.movinradio.services;

import java.util.List;
import java.util.Optional;

import com.movinradio.orm.entities.Playlist;

public interface PlaylistService {

	List<Playlist> findByOwner(Long ownerId);

	Playlist createPlaylist(Long ownerId, String title);

	boolean canLoggedUserEditPlaylistForUser(Long userId);

	Optional<Playlist> findById(Long id);

	Playlist updatePlaylist(Long playlistId, String title, List<Long> trackIds);

	boolean existsPlaylist(Long id);

	boolean canLoggedUserEditPlaylist(Long playlistId);

	void deletePlaylist(Long playlistId);

}
