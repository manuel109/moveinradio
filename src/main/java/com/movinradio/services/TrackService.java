package com.movinradio.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.movinradio.orm.entities.Track;

public interface TrackService {

	Page<Track> findPaginated(Pageable pageable);

	Optional<Track> findById(Long id);

	boolean existsTrack(Long trackId);

	Track createTrack(String author, String album, String title);

	Track updateTrack(Long id, String author, String album, String title);

	void delete(Long id);
}
