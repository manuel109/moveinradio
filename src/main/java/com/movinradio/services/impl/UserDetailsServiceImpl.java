package com.movinradio.services.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.movinradio.orm.repositories.UserRepository;
import com.movinradio.security.UserDetailsImpl;

@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		var user = userRepository.findByUsername(username).orElseThrow(
				() -> new UsernameNotFoundException("User Not Found with username : " + username));

		// dans un souci de simplicité, on ne s'occupe pas des 'authorities' (roles)
		// dans ce projet
		return new UserDetailsImpl(user.getId(), user.getUsername(), user.getPassword(), null);
	}

}
