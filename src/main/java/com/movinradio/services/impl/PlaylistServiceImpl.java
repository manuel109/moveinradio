package com.movinradio.services.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.movinradio.orm.entities.Playlist;
import com.movinradio.orm.entities.User;
import com.movinradio.orm.repositories.PlaylistRepository;
import com.movinradio.orm.repositories.UserRepository;
import com.movinradio.security.UserDetailsImpl;
import com.movinradio.services.PlaylistService;
import com.movinradio.services.TrackService;

@Service
public class PlaylistServiceImpl implements PlaylistService {

	@Autowired
	private TrackService trackService;

	@Autowired
	private PlaylistRepository playlistRepository;

	@Autowired
	private UserRepository userRepository;

	@Override
	public List<Playlist> findByOwner(Long ownerId) {
		var owner = userRepository.findById(ownerId);
		return owner.map(playlistRepository::findAllByOwner).orElse(List.of());
	}

	@Override
	public Playlist createPlaylist(Long ownerId, String title) {
		var owner = userRepository.findById(ownerId)
				.orElseThrow(() -> new UsernameNotFoundException("User not found with id : " + ownerId));
		var playlist = new Playlist();
		playlist.setOwner(owner);
		playlist.setTitle(title);
		return playlistRepository.save(playlist);
	}

	@Override
	public Optional<Playlist> findById(Long playlistId) {
		return playlistRepository.findById(playlistId);
	}

	@Override
	public boolean existsPlaylist(Long id) {
		return findById(id).isPresent();
	}

	@Override
	public Playlist updatePlaylist(Long playlistId, String title, List<Long> trackIds) {
		return findById(playlistId).map(p -> updatePlaylist(p, title, trackIds)).orElse(null);
	}

	private Playlist updatePlaylist(Playlist playlist, String title, List<Long> trackIds) {
		playlist.setTitle(title);
		playlist.getTracks().clear();
		playlist.getTracks().addAll(trackIds.stream().map(trackService::findById).map(Optional::get).toList());
		playlistRepository.save(playlist);
		return playlist;
	}

	@Override
	public void deletePlaylist(Long playlistId) {
		findById(playlistId).ifPresent(playlistRepository::delete);
	}

	@Override
	public boolean canLoggedUserEditPlaylistForUser(Long userId) {

		var authentication = SecurityContextHolder.getContext().getAuthentication();
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

		// si nous devions ajouter des roles, ici nous pourrions ajouter un contrôle et
		// autoriser un admin à faire l'édition

		return Objects.equals(userDetails.getId(), userId);
	}

	@Override
	public boolean canLoggedUserEditPlaylist(Long playlistId) {
		return findById(playlistId).map(Playlist::getOwner).map(User::getId).map(this::canLoggedUserEditPlaylistForUser)
				.orElse(false);
	}
}
