package com.movinradio.services.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.movinradio.orm.entities.Track;
import com.movinradio.orm.repositories.PlaylistRepository;
import com.movinradio.orm.repositories.TrackRepository;
import com.movinradio.services.TrackService;

@Service
@Transactional
public class TrackServiceImpl implements TrackService {

	@Autowired
	private PlaylistRepository playlistRepository;

	@Autowired
	private TrackRepository trackRepository;

	public Page<Track> findPaginated(Pageable pageable) {
		return trackRepository.findAll(pageable);
	}

	@Override
	public Track createTrack(String author, String album, String title) {
		var track = new Track();
		return update(track, author, album, title);
	}

	@Override
	public Optional<Track> findById(Long id) {
		return trackRepository.findById(id);
	}

	@Override
	public boolean existsTrack(Long trackId) {
		return findById(trackId).isPresent();
	}

	@Override
	public void delete(Long trackId) {
		findById(trackId).ifPresent(this::removeTrack);
	}

	private void removeTrack(Track track) {
		// ce code n'est clairement pas optimisé. Il faudrait revoir l'association
		// playlist <-> track et en faire une entité avec ses propres endpoints
		track.getPlaylist().forEach(p -> {
			p.getTracks().remove(track);
			playlistRepository.save(p);
		});
		trackRepository.delete(track);
	}

	@Override
	public Track updateTrack(Long id, String author, String album, String title) {
		return findById(id).map(t -> update(t, author, album, title)).orElse(null);
	}

	private Track update(Track track, String author, String album, String title) {
		track.setAlbum(album);
		track.setAuthor(author);
		track.setTitle(title);
		return trackRepository.save(track);
	}
}
