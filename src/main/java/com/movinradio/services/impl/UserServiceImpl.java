package com.movinradio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.movinradio.orm.entities.User;
import com.movinradio.orm.repositories.UserRepository;
import com.movinradio.services.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	/**
	 * Defined in {@see WebSecurityConfig}
	 */
	@Autowired
	private PasswordEncoder encoder;

	@Override
	public boolean alreadyExists(String username) {
		return userRepository.existsByUsername(username);
	}

	@Override
	public User createUser(String username, String password) {
		var user = new User();
		user.setUsername(username);
		user.setPassword(encoder.encode(password));
		return userRepository.save(user);
	}

}
