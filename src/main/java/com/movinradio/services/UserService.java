package com.movinradio.services;

import com.movinradio.orm.entities.User;

public interface UserService {

	boolean alreadyExists(String username);

	User createUser(String username, String password);

}
