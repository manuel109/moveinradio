package com.movinradio.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MovinradioBackendConfig {
	@Bean
	public ModelMapper modelMapper() {
		// fonctionne bien tel quel dans ce petit projet, pourrait être configuré en
		// mode strict pour être sûr
		return new ModelMapper();
	}
}
