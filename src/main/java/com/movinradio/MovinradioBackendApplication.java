package com.movinradio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovinradioBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovinradioBackendApplication.class, args);
	}

}
