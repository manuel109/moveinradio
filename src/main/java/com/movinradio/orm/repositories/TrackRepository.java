package com.movinradio.orm.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.movinradio.orm.entities.Track;

@Repository
public interface TrackRepository extends PagingAndSortingRepository<Track, Long> {

}
