package com.movinradio.orm.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.movinradio.orm.entities.Playlist;
import com.movinradio.orm.entities.User;

public interface PlaylistRepository extends CrudRepository<Playlist, Long> {

	List<Playlist> findAllByOwner(User owner);

}
