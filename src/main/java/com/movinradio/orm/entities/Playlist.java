package com.movinradio.orm.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "playlists")
public class Playlist {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "owner_id", nullable = false)
	private User owner;
	
	@Column(name = "title", nullable = false)
	private String title;

	/**
	 * N'ayant pas créé d'id pour l'association playlist <-> track, on ne peut avoir
	 * plusieurs fois la même track dans une playlist, ceci pourrait être amélioré.
	 */
	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(
		name = "playlist_tracks",
		joinColumns = { @JoinColumn(name = "playlist_id") },
		inverseJoinColumns = { @JoinColumn(name = "track_id") }
	)
	private List<Track> tracks;

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Track> getTracks() {
		return tracks;
	}

	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

	public Long getId() {
		return id;
	}

}
