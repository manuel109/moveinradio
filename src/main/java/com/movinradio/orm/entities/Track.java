package com.movinradio.orm.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tracks")
public class Track {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "author", nullable = false)
	private String author;

	/**
	 * Album peut être null dans le cas où on voudrait ajouter des podcasts par la
	 * suite
	 */
	@Column(name = "album", nullable = true)
	private String album;

	@Column(name = "title", nullable = false)
	private String title;

	/**
	 * N'ayant pas créé d'id pour l'association playlist <-> track, on ne peut avoir
	 * plusieurs fois la même track dans une playlist, ceci pourrait être grandement
	 * amélioré en créant une entité pour la relation.
	 */
	@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinTable(
		name = "playlist_tracks",
		inverseJoinColumns = { @JoinColumn(name = "playlist_id") },
		joinColumns = { @JoinColumn(name = "track_id") }
	)
	private List<Playlist> playlist;

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public List<Playlist> getPlaylist() {
		return playlist;
	}

}
