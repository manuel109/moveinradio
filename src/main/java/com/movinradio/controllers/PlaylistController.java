package com.movinradio.controllers;

import java.util.function.Predicate;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movinradio.controllers.requests.PlaylistRequest;
import com.movinradio.controllers.responses.MessageResponse;
import com.movinradio.controllers.responses.PlaylistResponse;
import com.movinradio.orm.entities.Playlist;
import com.movinradio.services.PlaylistService;
import com.movinradio.services.TrackService;

@RestController()
@RequestMapping(path = "/api/v1/playlists", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
class PlaylistController {

	@Autowired
	private PlaylistService playlistService;

	@Autowired
	private TrackService trackService;

	@Autowired
	private ModelMapper modelMapper;

	@PutMapping(value = "/{id}")
	public ResponseEntity<?> updatePlaylist(@PathVariable("id") Long id,
			@Valid @RequestBody PlaylistRequest playlistReq) {

		// check playlist existence
		if (!playlistService.existsPlaylist(id)) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse("Playlist not found with id : " + id));
		}

		// check user rights
		if (!playlistService.canLoggedUserEditPlaylist(id)) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN)
					.body(new MessageResponse("Logged user not authorized to create a playlist for another user"));
		}

		// ensure all provided tracks exist
		var missingTrackIds = playlistReq.getTrackIds().stream().filter(Predicate.not(trackService::existsTrack))
				.toList();
		if (!missingTrackIds.isEmpty()) {
			return ResponseEntity.badRequest()
					.body(new MessageResponse("Tracks with following ids not found : " + missingTrackIds));
		}

		var updatedPlaylist = playlistService.updatePlaylist(id, playlistReq.getTitle(), playlistReq.getTrackIds());

		return ResponseEntity.ok(toPlaylistResponse(updatedPlaylist));
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> deletePlaylist(@PathVariable("id") Long id) {

		// check playlist existence
		if (!playlistService.existsPlaylist(id)) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse("Playlist not found with id : " + id));
		}

		// check user rights
		if (!playlistService.canLoggedUserEditPlaylist(id)) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN)
					.body(new MessageResponse("Logged user not authorized to delete this playlist"));
		}

		playlistService.deletePlaylist(id);
		return ResponseEntity.noContent().build();
	}

	private PlaylistResponse toPlaylistResponse(Playlist playlist) {
		// this mapper could be configured to work in strict mode and ensure we don't
		// have issue but for now it is fine
		return modelMapper.map(playlist, PlaylistResponse.class);
	}

}
