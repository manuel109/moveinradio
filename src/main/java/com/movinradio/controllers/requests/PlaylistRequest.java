package com.movinradio.controllers.requests;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PlaylistRequest {

	@NotNull
	private Long ownerId;

	@NotBlank
	private String title;

	@NotNull
	private List<Long> trackIds;

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Long> getTrackIds() {
		return trackIds;
	}

	public void setTrackIds(List<Long> trackIds) {
		this.trackIds = trackIds;
	}

}
