package com.movinradio.controllers.requests;

import javax.validation.constraints.NotBlank;

public class TrackRequest {

	@NotBlank
	private String author;

	private String album;

	@NotBlank
	private String title;

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
