package com.movinradio.controllers.responses;

import java.util.List;

import javax.validation.constraints.NotBlank;

public class PlaylistResponse {

	private Long id;

	private Long ownerId;

	@NotBlank
	private String title;

	private List<TrackResponse> tracks;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<TrackResponse> getTracks() {
		return tracks;
	}

	public void setTracks(List<TrackResponse> tracks) {
		this.tracks = tracks;
	}

}
