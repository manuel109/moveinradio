package com.movinradio.controllers.responses;

import javax.validation.constraints.NotBlank;

public class TrackResponse {

	private Long id = -1L;

	@NotBlank
	private String author;

	private String album;

	@NotBlank
	private String title;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
