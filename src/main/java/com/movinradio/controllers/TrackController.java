package com.movinradio.controllers;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movinradio.controllers.requests.TrackRequest;
import com.movinradio.controllers.responses.MessageResponse;
import com.movinradio.controllers.responses.TrackResponse;
import com.movinradio.orm.entities.Track;
import com.movinradio.services.TrackService;

@RestController()
@RequestMapping(path = "/api/v1/tracks", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
class TrackController {

	@Autowired
	private TrackService trackService;

	@Autowired
	private ModelMapper modelMapper;

	@GetMapping
	public ResponseEntity<?> findAllPaginated(Pageable pageable) {
		var resultPage = trackService.findPaginated(pageable);
		if (resultPage == null || resultPage.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse(String.format("Page {} not found", pageable.getPageNumber())));
		}
		return ResponseEntity.ok(resultPage.map(this::toTrackResponse));
	}

	@PostMapping
	public ResponseEntity<TrackResponse> createTrack(@Valid @RequestBody TrackRequest trackReq) {
		// on devrait vérifier les autorisations de l'utilisateur connecté ici
		var persistedTrack = trackService.createTrack(trackReq.getAuthor(), trackReq.getAlbum(), trackReq.getTitle());
		return new ResponseEntity<TrackResponse>(toTrackResponse(persistedTrack), HttpStatus.CREATED);
	}

	@PutMapping(value = "/{id}")
	public ResponseEntity<?> updateTrack(@PathVariable("id") Long id, @Valid @RequestBody TrackRequest trackReq) {

		// check track existence
		if (!trackService.existsTrack(id)) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse("Track not found with id : " + id));
		}

		// on devrait vérifier les autorisations de l'utilisateur connecté ici

		var updatedTrack = trackService.updateTrack(id, trackReq.getAuthor(), trackReq.getAlbum(), trackReq.getTitle());
		return ResponseEntity.ok(toTrackResponse(updatedTrack));
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> deleteTrack(@PathVariable("id") Long id) {

		// check track existence
		if (!trackService.existsTrack(id)) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new MessageResponse("Track not found with id : " + id));
		}

		// on devrait vérifier les autorisations de l'utilisateur connecté ici

		trackService.delete(id);
		return ResponseEntity.noContent().build();
	}

	private TrackResponse toTrackResponse(Track track) {
		// this mapper could be configured to work in strict mode and ensure we don't
		// have issue
		// but for now it is fine
		return modelMapper.map(track, TrackResponse.class);
	}
}
