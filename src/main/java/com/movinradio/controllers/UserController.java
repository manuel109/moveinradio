package com.movinradio.controllers;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movinradio.controllers.requests.PlaylistRequest;
import com.movinradio.controllers.responses.MessageResponse;
import com.movinradio.controllers.responses.PlaylistResponse;
import com.movinradio.orm.entities.Playlist;
import com.movinradio.services.PlaylistService;

@RestController()
@RequestMapping(path = "/api/v1/users", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
class UserController {

	@Autowired
	private PlaylistService playlistService;

	@Autowired
	private ModelMapper modelMapper;

	@GetMapping(value = "/{id}/playlists")
	public ResponseEntity<?> findUserPlaylists(@PathVariable Long id) {

		if (!playlistService.canLoggedUserEditPlaylistForUser(id)) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN)
					.body(new MessageResponse("Logged user not authorized to access another user playlist"));
		}

		var playlists = playlistService.findByOwner(id);
		return ResponseEntity.ok(playlists);
	}

	@PostMapping(value = "/{id}/playlists")
	public ResponseEntity<?> createUserPlaylist(
			@PathVariable Long id,
			@Valid @RequestBody PlaylistRequest playlistReq) {

		if (!playlistService.canLoggedUserEditPlaylistForUser(playlistReq.getOwnerId())) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN)
					.body(new MessageResponse("Logged user not authorized to create a playlist for another user"));
		}

		var persistedPlaylist = playlistService.createPlaylist(playlistReq.getOwnerId(), playlistReq.getTitle());
		return new ResponseEntity<PlaylistResponse>(toPlaylistResponse(persistedPlaylist), HttpStatus.CREATED);
	}

	private PlaylistResponse toPlaylistResponse(Playlist playlist) {
		// this mapper could be configured to work in strict mode and ensure we don't
		// have issue but for now it is fine
		return modelMapper.map(playlist, PlaylistResponse.class);
	}

}
