package com.movinradio.controllers;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.server.LocalServerPort;

import com.movinradio.AbstractDbTest;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public abstract class AbstractControllerTest extends AbstractDbTest {

	@LocalServerPort
	private int port;

	protected String getBaseUrl() {
		return "http://localhost:" + port + "/api/v1/";
	}
}
