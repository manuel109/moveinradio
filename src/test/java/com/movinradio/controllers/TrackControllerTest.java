package com.movinradio.controllers;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.movinradio.controllers.requests.LoginRequest;
import com.movinradio.controllers.requests.SignupRequest;
import com.movinradio.controllers.requests.TrackRequest;
import com.movinradio.controllers.responses.JwtResponse;
import com.movinradio.controllers.responses.TrackResponse;

import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

@Testcontainers
class TrackControllerTest extends AbstractControllerTest {

	private static final String USERNAME = "Test user"; 
	private static final String PASSWORD = "Test password"; 

	private static String jwtToken;

	private void performAuthentication() {
		if (jwtToken == null) {
			var signupReq = new SignupRequest();
			signupReq.setUsername(USERNAME);
			signupReq.setPassword(PASSWORD);
	
			given().contentType(ContentType.JSON)
				.body(signupReq)
				.when().post(getBaseUrl() + "auth/signup")
				.then().statusCode(200);

			var loginReq = new LoginRequest();
			loginReq.setUsername(USERNAME);
			loginReq.setPassword(PASSWORD);
			
			jwtToken = given().contentType(ContentType.JSON)
					.body(loginReq)
					.when().post(getBaseUrl() + "auth/signin")
					.then().statusCode(200)
					.extract()
					.as(JwtResponse.class).getAccessToken();
		}
	}

	private RequestSpecification givenLogged() {
		performAuthentication();
		return given()
		          .headers(
		                  HttpHeaders.AUTHORIZATION,
		                  "Bearer " + jwtToken,
		                  HttpHeaders.CONTENT_TYPE,
		                  ContentType.JSON,
		                  HttpHeaders.ACCEPT,
		                  ContentType.JSON);
	}

	@Test
	void whenTracksAreRetrievedPaged_then200IsReceived() {
		givenLogged().when().get(getBaseUrl() + "tracks?page=0&size=5").then().statusCode(200);
	}

	@Test
	void whenTracksPageIsNotFound_then404IsReceived() {
		givenLogged().when().get(getBaseUrl() + "tracks?page=1000").then().statusCode(404);
	}

	@Test
	void whenCreatingTrackWithCorrectData_then201IsReceived() {
		var req = new TrackRequest();
		req.setAuthor("test author");
		req.setAlbum("test album");
		req.setTitle("test title");
		givenLogged()
			.body(req)
			.when().post(getBaseUrl() + "tracks")
			.then().statusCode(201)
			.body("id", equalTo(1))
			.body("author", equalTo(req.getAuthor()))
			.body("album", equalTo(req.getAlbum()))
			.body("title", equalTo(req.getTitle()));
	}

	@Test
	void whenCreatingTrackWithIncorrectData_then400IsReceived() {
		var req = new TrackRequest();
		givenLogged().contentType(ContentType.JSON)
			.body(req)
			.when().post(getBaseUrl() + "tracks")
			.then().statusCode(400);
	}

	@Test
	void whenDeletingFoundTrack_then204IsReceived() {
		// first, create a deletable track
		var req = new TrackRequest();
		req.setAuthor("test author");
		req.setAlbum("test album");
		req.setTitle("test title");
		var result = givenLogged().contentType(ContentType.JSON)
			.body(req)
			.when().post(getBaseUrl() + "tracks")
			.then().statusCode(201)
			.extract()
			.as(TrackResponse.class);

		// then delete it
		givenLogged().contentType(ContentType.JSON)
			.when().delete(getBaseUrl() + "tracks/" + result.getId())
			.then().statusCode(204);
	}

	@Test
	void whenDeletingNotFoundTrack_then404IsReceived() {
		givenLogged().contentType(ContentType.JSON)
			.when().delete(getBaseUrl() + "tracks/1234")
			.then().statusCode(404);
	}

	@Test
	void whenUpdatingFoundTrack_then200IsReceived() {
		// first, create an updatable track
		var req = new TrackRequest();
		req.setAuthor("test author");
		req.setAlbum("test album");
		req.setTitle("test title");
		var result = givenLogged().contentType(ContentType.JSON)
			.body(req)
			.when().post(getBaseUrl() + "tracks")
			.then().statusCode(201)
			.extract()
			.as(TrackResponse.class);

		// then update it
		req.setAuthor("new author");
		req.setAlbum("new album");
		req.setTitle("new title");
		givenLogged().contentType(ContentType.JSON)
			.body(req)
			.when().put(getBaseUrl() + "tracks/" + result.getId())
			.then().statusCode(200)
			.body("id", equalTo(result.getId().intValue()))
			.body("author", equalTo(req.getAuthor()))
			.body("album", equalTo(req.getAlbum()))
			.body("title", equalTo(req.getTitle()));
	}

	@Test
	void whenUpdatingNotFoundTrack_then404IsReceived() {
		var req = new TrackRequest();
		req.setAuthor("test author");
		req.setAlbum("test album");
		req.setTitle("test title");
		givenLogged().contentType(ContentType.JSON)
			.body(req)
			.when().put(getBaseUrl() + "tracks/1234")
			.then().statusCode(404);
	}
}
