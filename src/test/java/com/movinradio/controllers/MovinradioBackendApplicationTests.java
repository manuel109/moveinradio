package com.movinradio.controllers;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.movinradio.AbstractDbTest;
import com.movinradio.services.PlaylistService;
import com.movinradio.services.TrackService;
import com.movinradio.services.UserService;

/**
 * Placed in the controller package because of the package visibility of controllers
 * 
 * @author mpostec
 *
 */
@SpringBootTest
@Testcontainers
class MovinradioBackendApplicationTests extends AbstractDbTest {

	@Autowired
	private AuthController authController;

	@Autowired
	private PlaylistController playlistController;

	@Autowired
	private TrackController trackController;

	@Autowired
	private UserController userController;

	@Autowired
	private PlaylistService playlistService;

	@Autowired
	private TrackService trackService;

	@Autowired
	private UserService userService;

	@Test
	void contextLoads() {
		assertThat(authController).isNotNull();
		assertThat(playlistController).isNotNull();
		assertThat(trackController).isNotNull();
		assertThat(userController).isNotNull();
		assertThat(playlistService).isNotNull();
		assertThat(trackService).isNotNull();
		assertThat(userService).isNotNull();
	}

}
